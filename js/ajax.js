$(document).ready(function(){
	$('#example').on('click','button.switch',function(){
		$.ajax('resulttt.html',{
			beforeSend: function(){
				$('#status').text('Cargando...');
			}	
		})
		.done(function(response){
			$('#result').html(response);
		})
		.fail(function(request, errorType, errorMessage){
			console.log(errorType);
		})
		.always(function(){
			$('#status').text('Completado');
		})
	});


	$('#hasPets').on('change',function(){
		if($(this).is(':checked')){
			$('#pets-row').show();
		}
		else
		{	$('#pets-row').hide();
			$('#pet').val('');
			$('#pet-feedback').empty();	
		}
	});
	$('#hasPets').trigger('change');

	$('#pet').on('change',function(){
		var pet = $(this).val();
		if(pet == 'dog'){
			$('#pet-feedback').text('Dogs are great');
		}
		else if(pet == 'cat'){
			$('#pet-feedback').text('Cats are fun');
		}else{
			$('#pet-feedback').empty();
		}
	});
	$('#pet').trigger('change');

	//JQuery UI
	//Arrastrable
	$('#example').draggable();
});