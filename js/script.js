$(document).ready(function(){
	//Selector por etiqueta
	$('label').html('Hola');

	//Selector por Id
	$('#parrafo').html('Hola');

	//Selector por clase
	$('.clase1').text('Chao');

	//Seleccion Multiple
	$('.clase1, .clase2').html('<strong>Alerta</strong>');

	//Selectores Descendientes
	$('.listado li').addClass('highlighted');

	//Seleccion por Atributos
	$('input[required]').addClass('alerta');
	$('input[placeholder="email"]').addClass('alerta2');
	$('input[placeholder*="Name"]').addClass('alerta3');

	//Evento Click
	$('#boton').on('click', function(){
		$(this).text('Aceptar');
		$(this).toggleClass('alerta2');
	});

	//Evento Change con delegacion de Eventos
	$('#example').on('change','#place',function(){
		var selected = $('#place option:selected');
		if(selected != ''){
			$('#result').html(selected.val());
		}
	});

	//Eventos de teclado keydown
	$('#texto').on('keydown',function(){
		$('#result').html('Your Name is: ' + $(this).val());
	});

	//Eventos de teclado keyup se ejecuta al levantar el dedo de la tecla
	$('#texto').on('keyup',function(){
		$('#result').html('Your Name is: ' + $(this).val());
	});


	//Anadir HTML al DOM
	$('#addContainer').on('click','#boton',function(){
		//var value = $('#addContainer #itemInput').val();
		//var html = '<div class="item">' + value + '<div class="remove">X</div></div>'
		
		//Anadir al final del DOM
		//$('#placesContainer').append(html);
		//$(html).appendTo('#placesContainer');

		//Anadir al principio del DOM
		//$('#placesContainer').prepend(html);
		//$(html).prependTo('#placesContainer');

		// Inserta antes
		//$('#placesContainer').children().first().before(html);

		//Inserta Despues
		//$('#placesContainer').children().last().after(html);


		//$('#placesContainer').on('click','.remove', function(){
		//	var parent = $(this).parent().remove();	
		//});
	});

	$('#cont').text('Hola Mundo');
	console.log($('#cont').text());
	$('#cont').html('<strong>Hola</strong>');

	//Vacia el HTML
	$('#cont').empty();

	//Agregar atributo
	$('#cont').attr('atributo',123);

	//Animando Usando CSS
	$('#example1').on('click',function(){
		$(this).animate({opacity: 0.3},'fast',function(){ alert('Efecto') });	
	});

	//Animando Usando CSS
	$('#example2').on('click',function(){
		$(this).animate({left: 200, width: 50},'slow',function(){ alert('Efecto 2') });
	});

	//Animando Usando CSS
	$('#example3').on('click',function(){
		$(this).animate({left:'+=50', width: 50},'slow');
	});
});